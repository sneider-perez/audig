#!/usr/bin/env node
const yargs = require('yargs/yargs');
const {hideBin} = require('yargs/helpers');
const fs = require('fs');

const libCompleteName = 'audig-libs.jar';
const libCardsName = 'audig-lib.jar';

const readFile = (path) => {
    const encoding = 'utf8';
    try {
        return fs.readFileSync(path, encoding);
    } catch (e) {
        console.log(e.message);
    }
}

const writeFile = (path, content) => {
    try {
        return fs.writeFileSync(path, content);
    } catch (e) {
        console.log(e.message);
    }
}

const modifyProjectComplete = () => {
    removeGradleDependencies(true);
    changeGradleWrapper(true);
    copyLibraries(true);
}

const modifyProject = () => {
    removeCardsDependencies(true);
    copyLibraries(false);
}

const resetProjectComplete = () => {
    removeGradleDependencies(false);
    changeGradleWrapper(false);
    deleteLibraries();
}

const resetProject = () => {
    removeCardsDependencies(false);
    deleteLibraries();
}

const deleteLibraries = () => {
    const dir = './libs';
    if (fs.existsSync(dir)) {
        fs.rmSync(dir, {recursive: true});
    }
}

const copyLibraries = (complete) => {
    const dir = './libs';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    const libName = complete ? libCompleteName : libCardsName;
    fs.copyFileSync('C://audig/' + libName, './libs/' + libName)
}

const removeGradleDependencies = (modify) => {
    const path = './build.gradle';

    const replaceArray = [
        {
            search: "https://artifactory.apps.bancolombia.com/gradlew/",
            replace: "https://services.gradle.org/distributions/"
        },
        {
            search: "maven { url 'https://artifactory",
            replace: "//maven { url 'https://artifactory"
        },
        {
            search: " implementation(group: 'co.com.bancolombia'",
            replace: " //implementation(group: 'co.com.bancolombia'"
        },
        {
            search: " implementation(group: 'software.amazon.awssdk', name: 'bom',",
            replace: " //implementation(group: 'software.amazon.awssdk', name: 'bom',"
        },
        {
            search: " implementation(group: 'com.amazonaws', name: 'aws-java-sdk',",
            replace: " //implementation(group: 'com.amazonaws', name: 'aws-java-sdk',"
        },
        {
            search: "implementation group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: '2.12.1'",
            replace: "implementation group: 'com.fasterxml.jackson.core', name: 'jackson-core', version: '2.12.1'\n" +
                "    implementation group: 'com.fasterxml.jackson.core', name: 'jackson-annotations', version: '2.12.1'\n" +
                "    implementation group: 'com.fasterxml.jackson.core', name:'jackson-databind', version: '2.12.1'\n" +
                "    implementation 'com.fasterxml.jackson.datatype:jackson-datatype-jdk8:2.12.1'\n" +
                "    implementation 'com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.12.1'"
        }
    ];

    let content = readFile(path);
    if (content) {
        content = replaceWithArray(content, replaceArray, modify);
        content = addLibJarDependencies(content, modify, libCompleteName);

        writeFile(path, content);
        return true;
    }
    return false;
}

const removeCardsDependencies = (modify) => {
    const path = './build.gradle';

    const replaceArray = [
        {
            search: " implementation(group: 'co.com.bancolombia', name: 'consult",
            replace: " //implementation(group: 'co.com.bancolombia', name: 'consult"
        },
        {
            search: " implementation(group: 'co.com.bancolombia', name: 'cardSecurity",
            replace: " //implementation(group: 'co.com.bancolombia', name: 'cardSecurity"
        },
        {
            search: " implementation(group: 'co.com.bancolombia', name: 'encryptionUtility",
            replace: " //implementation(group: 'co.com.bancolombia', name: 'encryptionUtility"
        }
    ];

    let content = readFile(path);
    if (content) {
        content = replaceWithArray(content, replaceArray, modify);
        content = addLibJarDependencies(content, modify, libCardsName);

        writeFile(path, content);
        return true;
    }
    return false;
}

const replaceWithArray = (content, replaceArray, modify) => {
    replaceArray.forEach(value => {
        const searchKey = modify ? value['search'] : value['replace'];
        const replaceKey = modify ? value['replace'] : value['search'];
        content = content.replaceAll(searchKey, replaceKey);
    });
    return content;
}

const addLibJarDependencies = (content, modify, libName) => {
    const oSearchKey = "dependencies {";
    const oReplaceKey = "dependencies {\n\n    //Bancolombia libs\n    compileOnly files('libs/" + libName + "')\n" +
        "    testRuntimeOnly files('libs/" + libName + "')\n" +
        "    testCompileOnly files('libs/" + libName + "')\n" +
        "    implementation 'com.google.code.gson:gson:2.8.9'\n";

    if (modify) {
        if (!content.includes('//Bancolombia libs')) {
            content = content.replaceAll(oSearchKey, oReplaceKey);
        }
    } else {
        content = content.replaceAll(oReplaceKey, oSearchKey);
    }

    if (!content.includes('//Bancolombia libs')) {
        const searchKey = modify ? oSearchKey : oReplaceKey;
        const replaceKey = modify ? oReplaceKey : oSearchKey;

        content = content.replaceAll(searchKey, replaceKey);
    }
    return content;
}

const changeGradleWrapper = (modify) => {
    const path = './gradle/wrapper/gradle-wrapper.properties';

    const replaceArray = [
        {
            search: 'https\\://artifactory.apps.bancolombia.com/gradlew/',
            replace: "https\\://services.gradle.org/distributions/"
        }
    ];

    let content = readFile(path);
    if (content) {
        content = replaceWithArray(content, replaceArray, modify);
        writeFile(path, content);
        return true;
    }
    return false;
}

yargs(hideBin(process.argv))
    .command('modify', 'Modify project local', (yargs) => {
        return yargs;
    }, (argv) => {
        if (argv.complete) {
            modifyProjectComplete();
        } else {
            modifyProject();
        }
    })
    .option('complete', {
        alias: 'c',
        type: 'boolean',
        description: 'Remove all external libs'
    })
    .command('reset', 'Reset project changes', (yargs) => {
        return yargs;
    }, (argv) => {
        if (argv.complete) {
            resetProjectComplete();
        } else {
            resetProject();
        }
    })
    .option('complete', {
        alias: 'c',
        type: 'boolean',
        description: 'Remove all external libs'
    })
    .parse();
